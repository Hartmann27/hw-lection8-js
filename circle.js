        // При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
        // Данная кнопка должна являться единственным контентом в теле HTML документа, 
        // весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
        // При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. 
        // При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
        // При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
        //  то есть все остальные круги сдвигаются влево.
    
        const btn = document.getElementById("btn");


        btn.onclick = () => {
            const input = document.createElement("input");
            input.setAttribute("placeholder", "write diameter");
            document.body.append(input);
        
            const btnCircle = document.createElement("button");
            btnCircle.innerText = "draw";
            document.body.append(btnCircle);
            
            const circles = document.createElement("div");
            circles.setAttribute("id", "circles");
            document.body.append(circles);
        
            btnCircle.onclick = () => {
                for (let i = 0 ; i<100 ; i++) {
                    const circle = document.createElement("div");
                    circle.style.width = `${input.value}px`;
                    circle.style.height = `${input.value}px`;
                    circle.style.borderRadius = `${input.value}px`;
                    circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, ${Math.floor(Math.random() * 100) + '%'} ,${Math.floor(Math.random() * 60) + '%'})`;
                    circles.append(circle);
        
                    circle.onclick = (element) =>{
                        element.target.remove();
                    };
                };
        
        
            };
            
        };